#
# Makefile personnalisé utilisé avec Geany et/ou un terminal.
#

# Shell par défaut.
SHELL=/bin/sh

# Compilateur.
CC?=cc

# Nom de l'exécutable en sortie.
APP_NAME=ptrarr

# Le/les fichiers source à compiler.
SRCS=$(wildcard *.c)

# Fichier de sortie.
OUT=-o $(APP_NAME)

# Optimisé, sans information de débogage.
CRELEASE=-DNDEBUG -Ofast -s

# Débogage.
CDEBUG=-Og -g

# Options de compilation.
CFLAGS=-Wall -Wextra -Werror -std=gnu11 -march=native -D_GNU_SOURCE

# Liaisons, bibliothèques et inclusions.
CPPFLAGS=
LDFLAGS=

# Cibles ne correspondant à aucun fichier du répertoire en cours.
.PHONY: release debug clean clobber

# Cible par défaut
.DEFAULT_GOAL=release

 
# Compilation.
release: $(SRCS)
	$(CC) $(CRELEASE) $(CFLAGS) $(OUT) $^
	
debug: $(SRCS)
	$(CC) $(CDEBUG) $(CFLAGS) $(OUT) $^
 
# Nettoyage.
clean:
	$(RM) *.h~
	$(RM) *.c~
	$(RM) Makefile~

clobber: clean
	$(RM) $(APP_NAME)

