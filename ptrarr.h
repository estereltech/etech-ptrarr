/*

	Projet		: Tableau dynamique de pointeurs : ptrarr_t.
	Auteur		: © Philippe Maréchal
	Date		: 28 novembre 2016
	Version		: 1.0

	Notes		:

	Révisions	:
	29 novembre 2016 > une seule fonction de rappel.
	30 novembre 2016 > la/les fonction(s) de rappel sont externes.
	1er janvier 2017 > ptrarr_remove > ptrarr_remove_at.
	12 avril 2017 > fonction de tri 'ptrarr_sort' et 3 fonctions de rappel.
	13 avril 2017 > ptrarr_traverse et ptrarr_traverse_func + ptrarr_find et
		ptrarr_find_func.
	14 avril 2017 > 5 signatures de fonctions de rappel et 'ptrarr_edit_func'.
	15 avril 2017 > nouvelle fonction 'ptrarr_remove'.
	19 avril 2017 > corrections et simplification de la fonction 'ptrarr_sort'.
	20 avril 2017 > fonction 'ptrarr_edit_at'.
	14 juillet 2017 > simplification de la fonction '_reindex_list'.
	21 avril 2018 > ptrarr_traverse devient ptrarr_for_each.
	08 décembre 2018 > modification mineure dans l'emploi de realloc.

*/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */


#pragma once

#include <stdlib.h>
#include <stdbool.h>

typedef struct stptrarr *ptrarr_t;

/*
 * Les 4 signatures de fonctions spécialisées qui sont nécessaires :
 * 1 - libération des données (blocs mémoire alloués).
 * 2 - comparaison / tri.
 * 3 - traversée de la liste.
 * 4 - recherche selon critère.
 * 5 - modification d'un élément.
 * Ces fonctions sont à définir dans le module utilisant ptrarr.
 */
typedef void 	(*ptrarr_free_func)(void *data);
typedef int 	(*ptrarr_comp_func)(const void *a, const void *b);
typedef void 	(*ptrarr_for_each_func)(const void *data);
typedef bool 	(*ptrarr_find_func)(const void *data, const void *cr);
typedef bool 	(*ptrarr_edit_func)(const void *data, const void *new_data);


// Constructeur/destructeur.
ptrarr_t
ptrarr_new(void);

// Note : ptrarr_free_func doit être fournie.
void
ptrarr_free(ptrarr_t pa, ptrarr_free_func ff);

// Ajout/suppression.
bool
ptrarr_add(ptrarr_t pa, void *data);
bool
ptrarr_remove_at(ptrarr_t pa, size_t idx, ptrarr_free_func ff);
bool
ptrarr_remove(ptrarr_t pa, void *elem, ptrarr_free_func ff);

// Liste, taille et donnée selon indice.
void**
ptrarr_list(ptrarr_t pa, size_t *sz);
size_t
ptrarr_size(ptrarr_t pa);
void*
ptrarr_data_at(ptrarr_t pa, size_t idx);

// Tri selon une fonction de rappel.
void
ptrarr_sort(ptrarr_t pa, ptrarr_comp_func cf);

// Traversée de la liste.
void
ptrarr_for_each(ptrarr_t pa, ptrarr_for_each_func ff);

/*
 * Retrouver la première occurence d'un élément selon un critère et une
 * fonction de recherche spécialisée.
 */
void*
ptrarr_find(ptrarr_t pa, void *cr, ptrarr_find_func ff);

/*
 * Modification par critère(s) et indice.
 * Une fonction spécialisée 'ptrarr_edit_func' doit être implémentée.
 */
bool
ptrarr_edit(ptrarr_t pa,
			void *cr,
			void *new,
			ptrarr_find_func ff,
			ptrarr_edit_func ef);
bool
ptrarr_edit_at(struct stptrarr *pa,
				size_t idx,
				void *new,
				ptrarr_edit_func ef);
