/*

	Projet    : Tests du module ptrarr[h,c].
	Auteur    : © Philippe Maréchal
	Date      : 28 novembre 2016
	Version   :

	Notes     :

	Révisions :

*/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */


#include <stdio.h>
#include <string.h>

#include "persons.h"


#define __unused 	__attribute__((unused))
#define __used 		__attribute__((used))


int
main(int __unused argc, char __unused **argv)
{
	printf("\nDébut du programme.\n\n");

	Persons ps = persons_new();
	persons_add_new(ps, "Mozart", "Wolfgang");
	persons_add_new(ps, "Van Beethoven", "Ludwig");
	persons_add_new(ps, "Brahms", "Johannes");
	persons_add_new(ps, "Jolivet", "André");
	persons_add_new(ps, "Dutilleux", "Henri");

	printf("Liste des %lu personnes :\n", persons_size(ps));
	persons_for_each(ps);
	printf("\n");

	size_t idx = 2;
	printf("Suppression de la  personne à l'indice %lu :\n", idx);
	persons_remove_at(ps, idx);
	printf("\n");

	printf("Liste des %lu personnes :\n", persons_size(ps));
	persons_for_each(ps);

	struct stperson p = {"Jolivet", "André"};
	ssize_t sidx = persons_data_index(ps, &p);
	printf("Indice de 'Jolivet André' = %ld\n", sidx);

	printf("\nTri par prénom :\n");
	persons_sort(ps);
	persons_for_each(ps);

	idx = 3;
	printf("\nModification de la  personne à l'indice %lu :\n", idx);
	struct stperson pe = {"Mozart", "Wolfgang Amadeus"};
	persons_edit_at(ps, idx, &pe);
	printf("\n");

	printf("Liste des %lu personnes :\n", persons_size(ps));
	persons_for_each(ps);

	persons_free(ps);

	printf("\nFin du programme.\n\n");

	return EXIT_SUCCESS;
}

