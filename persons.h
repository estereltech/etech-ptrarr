/*

	Projet    : Tableau dynamique de pointeurs vers objets 'Person'.
	Auteur    : © Philippe Maréchal
	Date      : 29 novembre 2016
	Version   :

	Notes     :
	L'espace public ne voit pas la liste (void**) du ptrarr.

	Révisions :
	1er janvier 2017 > nouvelle fonction 'persons_data_index'.
	21 avril 2018 > 'persons_compare_func' est maintenant static (privée).
	04 avril 2019 > ajout de 'persons_edit_at' et 'person_edit_func'.

*/


#pragma once

#include "ptrarr.h"

#define NLEN 32

typedef struct stperson {
	char *lname;
	char *fname;
} *Person;

typedef enum {
	CR_FNAME,
	CR_LNAME
} crit;

typedef struct stpersons *Persons;

// Person.
Person
person_new(const char *lname, const char *fname);
void
person_free(Person p);
size_t
person_size(Person p);
void**
persons_list(Persons pe);

// Persons.
Persons
persons_new(void);
void
persons_free(Persons pe);

void
persons_add(Persons pe, Person p);
bool
persons_add_new(struct stpersons *pe, const char *lname, const char *fname);
void
persons_remove_at(Persons pe, size_t idx);
bool
persons_edit_at(Persons pe, size_t idx, void *new);

void
persons_for_each(Persons pe);
size_t
persons_size(Persons pe);
Person
persons_at(Persons pe, size_t idx);
ssize_t
persons_data_index(Persons pe, Person p);
void
persons_sort(Persons pe);

