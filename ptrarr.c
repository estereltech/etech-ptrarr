#include "ptrarr.h"

#include <stdio.h>
#include <string.h>


//// Privé.


/*
 * Note : la structure 'stptrarr' occupe 16 octets en mémoire sur architecture
 * 64 bits.
 * - void **list : le tableau de pointeurs.
 * - size_t size : le nombre d'éléments.
 */
struct stptrarr
{
	void **list;
	size_t size;
};

/*
 * Réindiçage du tableau de pointeurs après suppression d'un élément.
 * Note 1 : idx est l'indice de lélément supprimé.
 * Si l'élément supprimé n'est pas le dernier il est nécessaire de décaler les
 * indices de tous ceux qui sont au dessus de lui d'un rang vers le bas.
 * Note 2 : l'opération est effectuée sur des pointeurs (les données ne sont
 * pas copiées ou échangées) et est donc très rapide quelque soit le nombre
 * d'éléments.
 */
static void
_reindex_list(struct stptrarr *pa, size_t idx)
{
	if ( idx < pa->size - 1 ) {
		for (size_t i = idx + 1; i < pa->size; ++i) {
			pa->list[i - 1] = pa->list[i];
		}
	}

	/*
	 * Décrémenter du nombre d'éléments après le réindiçage puisqu'un élément
	 * du tableau a été supprimé.
	 */
	pa->size--;
}


//// Public.


// Constructeur.
struct stptrarr*
ptrarr_new(void)
{
	struct stptrarr *pa = malloc(sizeof(*pa));
	if ( pa == NULL ) {
		return NULL;
	}

	pa->list = NULL;
	pa->size = 0;

	return pa;
}

/*
 * Suppression de toutes les ressources (allocations) pour le tableau.
 * Note : la fonction spécialisée 'ff' est utilisée ici.
 */
void
ptrarr_free(struct stptrarr *pa, ptrarr_free_func ff)
{
	for (size_t i = 0; i < pa->size; ++i) {
		ff(pa->list[i]);
		pa->list[i] = NULL;
	}

	free(pa->list);
	pa->list = NULL;

	free(pa);
	pa = NULL;
}

bool
ptrarr_add(struct stptrarr *pa, void *data)
{
	/*
	 * Note : pa->list est initialisé à NULL dans le constructeur donc même
	 * s'il s'agit du premier élément l'utilisation de 'realloc' est pertinente.
	 */
	void *nptr = (void*)realloc(pa->list, (pa->size + 1) * sizeof(void*));
	if ( NULL == nptr ) {
		return false;
	}
	pa->list = nptr;
	pa->list[pa->size] = data;
	pa->size++;

	return true;
}

/*
 * Retourne la liste de pointeurs vers structures et le nombre (*sz) d'
 * éléments si besoin.
 */
void**
ptrarr_list(struct stptrarr *pa, size_t *sz) {
	if ( sz ) {
		*sz = pa->size;
	}

	return pa->list;
}

// Retourne uniquement le nombre d'éléments.
size_t
ptrarr_size(struct stptrarr *pa) {
	return pa->size;
}

// Retourne les données à l'indice idx sous forme de pointeur void*.
void*
ptrarr_data_at(struct stptrarr *pa, size_t idx)
{
	if ( idx >= pa->size ) {
		return NULL;
	}

	return pa->list[idx];
}

// Suppression d'un élément par son indice.
bool
ptrarr_remove_at(struct stptrarr *pa,
				size_t idx,
				ptrarr_free_func ff)
{
	// Si l'indice n'est pas valide.
	if ( idx >= pa->size ) {
		return false;
	}

	// Si l'indice n'est pas valide.
	if ( ! ptrarr_data_at(pa, idx) ) {
		return false;
	}

	// Suppression des ressources allouées pour l'élément.
	ff(pa->list[idx]);
	pa->list[idx] = NULL;

	// Réindiçage de la liste si nécessaire (voir '_reindex_list').
	_reindex_list(pa, idx);

	return true;
}

/*
 * Suppression d'un élément par son pointeur et comparaison des adresses en
 * mémoire.
 * La libération des ressources allouées est effectuée par une fonction
 * spécialisée à redéfinir dans l'application.
 */
bool
ptrarr_remove(struct stptrarr *pa,
				void *elem,
				ptrarr_free_func ff)
{
	// Si l'élément n'est pas valide.
	if ( elem == NULL ) {
		return false;
	}

	for (size_t i = 0; i < pa->size; ++i) {
		if ( elem == pa->list[i] ) {
			
			// printf("%p = %p\n", elem, pa->list[i]);
			
			// Suppression des ressources allouées pour l'élément.
			ff(pa->list[i]);
			pa->list[i] = NULL;

			// Réindiçage de la liste si nécessaire.
			_reindex_list(pa, i);

			return true;
		}
	}

	return false;
}

/*
 * Fonction de tri par qsort; la fonction spécialisée doit être implémentée.
 */
void
ptrarr_sort(struct stptrarr *pa,
			ptrarr_comp_func cf)
{
	if ( pa->size <= 1 ) {
		return;
	}

	qsort(pa->list, pa->size, sizeof(pa->list[0]), cf);
}

void
ptrarr_for_each(struct stptrarr *pa,
				ptrarr_for_each_func ff)
{
	for(size_t i = 0; i < pa->size; ++i) {
		ff(pa->list[i]);
	}
}

void*
ptrarr_find(struct stptrarr *pa,
			void *cr,
			ptrarr_find_func ff)
{
	for (size_t i = 0; i < pa->size; ++i) {
		if ( ff(pa->list[i], cr) ) {
			return pa->list[i];
		}
	}

	return NULL;
}

/*
 * Modification d'un élément selon critère(s) et fonctions spécialisée.
 * - ptrarr_find_func permet de retrouver l'élément à modifier.
 * - ptrarr_edit_func procède à la modification de l'élément.
 */
bool
ptrarr_edit(struct stptrarr *pa,
			void *cr,
			void *new,
			ptrarr_find_func ff,
			ptrarr_edit_func ef)
{
	for (size_t i = 0; i < pa->size; ++i) {
		if ( ff(pa->list[i], cr) ) {
			return ( ef(pa->list[i], new) );
		}
	}

	return false;
}

/*
 * Modification d'un élément selon indice et sa fonction spécialisée.
 * - ptrarr_edit_func procède à la modification de l'élément.
 */
bool
ptrarr_edit_at(struct stptrarr *pa,
				size_t idx,
				void *new,
				ptrarr_edit_func ef)
{
	void *found = NULL;
	if ( (found = ptrarr_data_at(pa, idx)) ) {
		return ( ef(found, new) );
	}
	
	return false;
}
