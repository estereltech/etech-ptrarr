#include "persons.h"

#include <stdio.h>
#include <string.h>


//// Privé.


// Intégration du ptrarr générique dans la structure.
struct stpersons {
	ptrarr_t ar;
};

/*
 * Définition de la fonction spécialisée pour la libération des ressources
 * allouées. Cette fonction est spécifique à 'struct stpersons' ou au module
 * qui incorpore un type 'ptrarr_t'.
 */
static void
person_free_func(void *data)
{
	person_free((Person)data);
}

/*
 * Définition de la fonction spécialisée pour la comparaison (tri).
 */
static int
persons_compare_func(const void *v1, const void *v2)
{
	struct stperson *p1 = *(struct stperson**)v1;
	struct stperson *p2 = *(struct stperson**)v2;
	
	return strcmp(p1->fname, p2->fname);
}

/*
 * Définition de la fonction spécialisée pour la modification.
 */
static bool
person_edit_func(const void *data, const void *new_data)
{
	struct stperson *p = (struct stperson*)data;
	free(p->fname);
	p->fname = NULL;
	free(p->lname);
	p->lname = NULL;

	p->fname = strdup(((struct stperson*)new_data)->fname);
	p->lname = strdup(((struct stperson*)new_data)->lname);

	return (p->fname && p->lname);
}


//// Public.


// L'élément (structure) : Person.
struct stperson*
person_new(const char *lname, const char *fname) {
	struct stperson *p = (struct stperson*)malloc(sizeof(*p));
	if ( ! p ) {
		return NULL;
	}

	p->lname = strndup(lname, NLEN);
	p->fname = strndup(fname, NLEN);

	return p;
}

void
persons_sort(struct stpersons *pe)
{
	ptrarr_sort(pe->ar, persons_compare_func);
}

void
person_free(struct stperson *p) {
	free(p->lname);
	free(p->fname);
	free(p);
}

size_t
person_size(Person p)
{
	return (strlen(p->lname) + strlen(p->fname));
}

// Le tableau : Persons.
struct stpersons* persons_new(void) {
	struct stpersons *pe = (struct stpersons*)malloc(sizeof(*pe));
	if ( ! pe ) {
		return NULL;
	}

	// Intégration du ptrarr générique.
	pe->ar = ptrarr_new();
	if ( ! pe->ar ) {
		free(pe);

		return NULL;
	}

	return pe;
}

void
persons_free(struct stpersons *pe) {
	ptrarr_free(pe->ar, person_free_func);
	
	free(pe);
}

void
persons_add(struct stpersons *pe, struct stperson *p) {
	ptrarr_add(pe->ar, p);
}

bool
persons_add_new(struct stpersons *pe, const char *lname, const char *fname) {
	struct stperson *p = person_new(lname, fname);
	if ( p ) {
		ptrarr_add(pe->ar, p);

		return true;
	}

	return false;
}

void
persons_for_each(struct stpersons *pe) {
	size_t s = 0;
	void **list = ptrarr_list(pe->ar, &s);
	
	for (size_t i = 0; i < s; ++i) {
		printf("%lu - %s %s\n",
			i,
			((struct stperson*)list[i])->lname,
			((struct stperson*)list[i])->fname
			);
	}
}

size_t
persons_size(struct stpersons *pe) {
	return ptrarr_size(pe->ar);
}

void**
persons_list(struct stpersons *pe) {
	return ptrarr_list(pe->ar, NULL);
}

void
persons_remove_at(struct stpersons *pe, size_t idx) {
	ptrarr_remove_at(pe->ar, idx, person_free_func);
}

bool
persons_edit_at(struct stpersons *pe, size_t idx, void *new)
{
	return ptrarr_edit_at(pe->ar, idx, new, person_edit_func);
}

struct stperson*
persons_at(struct stpersons *pe, size_t idx) {
	return ptrarr_data_at(pe->ar, idx);
}

ssize_t
persons_data_index(Persons pe, Person p)
{
	size_t s = 0;
	void **list = ptrarr_list(pe->ar, &s);
	for (size_t i = 0; i < s; ++i) {
		if ( strcmp(((struct stperson*)list[i])->lname, p->lname) == 0 &&
			 strcmp(((struct stperson*)list[i])->fname, p->fname) == 0 ) {
			return i;
		}
	}

	return -1;
}

