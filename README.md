**module C ptrarr[h,c].**  
Tableau dynamique de pointeurs.

*Pour construire le programme en mode optimisé :*  
$ make

*Pour construire le programme en mode de débogage :*  
$ make debug

*Pour exécuter le programme :*  
$ ./ptrarr
